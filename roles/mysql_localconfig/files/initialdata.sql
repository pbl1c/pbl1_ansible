-- MySQL dump 10.13  Distrib 5.7.17, for Linux (x86_64)
--
-- Host: localhost    Database: pbl1
-- ------------------------------------------------------
-- Server version	5.7.17-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
DROP TABLE IF EXISTS `may`;
DROP TABLE IF EXISTS `june`;
DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;


CREATE TABLE `student` (
  `id` varchar(7) NOT NULL,
  `password` varchar(10) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('0K01007','0K01007','琴浦');
INSERT INTO `student` VALUES ('0K01014','0K01014','西本');
INSERT INTO `student` VALUES ('0K01016','0K01016','三浦');
INSERT INTO `student` VALUES ('0K01019','0K01019','宮松');
INSERT INTO `student` VALUES ('0K01015','0K01015','早勢');
INSERT INTO `student` VALUES ('0K01023','0K01023','神');
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;




CREATE TABLE `june` (
  `id` varchar(7) NOT NULL,
  `name` varchar(20),
  `day` varchar(2),
  `time1` varchar(5),
  `time2` varchar(5),
  `code1` varchar(1),
  `code2` varchar(3),
  `code3` varchar(1),
  `t1` varchar(10),
  `t2` varchar(10),
  `t3` varchar(10),
  `t4` varchar(10),
  `t5` varchar(10)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `june` WRITE;
/*!40000 ALTER TABLE `june` DISABLE KEYS */;
INSERT INTO `june` VALUES ('0K01006','kotoura','15','09:32','15:34','2','1','3','出席','出席','出席','出席','欠席');
/*!40000 ALTER TABLE `june` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;





CREATE TABLE `teacher` (
  `id` varchar(7) NOT NULL,
  `name` varchar(10) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
INSERT INTO `teacher` VALUES ('teacher','教員','teacher');
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;










-- Dump completed on 2017-04-24 16:36:14
